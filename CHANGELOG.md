# Changelog

## CoLib.Common.EF

### 2.0.0

- CHANGE: Project extracted into separate Github-project
- CHANGE: Projekt supports .NET 4.5+ only. Redundancy removed from dependencies
- CHANGE: Dependency EntityFramework changed from 6.2.0 to 6.0.0

### 1.1.1

- NEW: Icon added (#7). Will be published with next version of this Nuget-Package 

### 1.1.0

- NEW: Support of .Net-Framework 4.5.2, 4.6.2 and 4.7.1

### 1.0.0

- NEW: DbModelBuilderExtension to use TablePrefix